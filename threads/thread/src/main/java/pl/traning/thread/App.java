package pl.traning.thread;

public class App 
{
    public static void main( String[] args )
    {
    	Calculator calculator = new Calculator();
    	new Reader(calculator).start();
    	new Reader(calculator).start();
    	new Reader(calculator).start();
    	calculator.start();
    }
}
